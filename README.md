# OGN Tracker using Arduino core

Port the existing OGN Tracker software to run on Arduino ATmega32 core.
For this project there is also a fixed hardware: A PCB board that defines the pins and ports for peripherals. Changing those to fit your own Arduino design should be easy.