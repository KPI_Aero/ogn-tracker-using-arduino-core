
#include <SPI.h>      // SPI port
#include <Wire.h>     // I2C port

#define PIN_PCB_LED    13    // LED on the PCB

#define PIN_RFM_RESET   4    // RFM69 RESET
#define PIN_RFM_CS      8    // RFM69 Chip-Select
#define PIN_RFM_IRQ     3    // RFM69 DIO0

#define PIN_PPS         2    // PPS input

bool GPS_PPS_isOn(void) { return digitalRead(PIN_PPS); }

int GPS_UART_Read(uint8_t &Byte)  // read byte from the GPS serial port
{ int Ret=Serial1.read();
  if(Ret>=0) Byte=Ret;
  return Ret; }

void GPS_UART_Write(char Byte) // write byte to the GPS serial port
{ Serial1.write(Byte); }

void  GPS_UART_SetBaudrate(int BaudRate) { Serial1.begin(BaudRate); }

// SemaphoreHandle_t CONS_Mutex; // console port Mutex

void CONS_UART_Write(char Byte) // write byte to the console (USB serial port)
{ Serial.write(Byte); }

int  CONS_UART_Free(void)
{ return Serial.availableForWrite(); }

int  CONS_UART_Read (uint8_t &Byte)
{ int Ret=Serial.read(); if(Ret<0) return 0;
  Byte=Ret; return 1; }

void IO_Configuration(void)  // GPIO for LED and RF chip, SPI for RF, ADC, GPS GPIO and IRQ
{ pinMode(PIN_PCB_LED,   OUTPUT);  // digital pin 13 as an output = LED on the PCB

  pinMode(PIN_RFM_RESET, OUTPUT);  // RF chip reset
  pinMode(PIN_RFM_IRQ,    INPUT);  // RF chip IRQ request (packet arrived on RX or packet completed on TX)
  pinMode(PIN_RFM_CS,    OUTPUT);  // RF chip SPI select (low-active)
  SPI.begin();
  SPI.beginTransaction(SPISettings(4000000, MSBFIRST, SPI_MODE0));

  pinMode(PIN_PPS, INPUT);         // PPS input

  Serial1.begin(4800);             // GPS/MAVlink serial port
  // CONS_Mutex = xSemaphoreCreateMutex();

  Wire.begin();                     // strat I2C (pins are fixed)
}

// --------------------------------------------------------------------------------
// LED on the PCB

void LED_PCB_On   (void) { digitalWrite(PIN_PCB_LED, HIGH); }
void LED_PCB_Off  (void) { digitalWrite(PIN_PCB_LED,  LOW); }

volatile uint8_t LED_PCB_Counter = 0;

void LED_PCB_Flash(uint8_t Time) { LED_PCB_Counter=Time; } // [ms]

static void LED_PCB_TimerCheck(void)
{ uint8_t Counter=LED_PCB_Counter;
  if(Counter)
  { Counter--;
    if(Counter) LED_PCB_On();
           else LED_PCB_Off();
    LED_PCB_Counter=Counter; }
}

// --------------------------------------------------------------------------------

void setup()
{ IO_Configuration();
  // Open serial communications and wait for port to open:
  Serial.begin(115200);                   // console/debug UART
  // while (!Serial) { }                  // wait for USB serial port to connect
  Serial.println("OGN Tracker on ATmega2560");
}

void loop()
{ LED_PCB_On();
  delay(100);
  LED_PCB_Off();
  delay(900);
}
